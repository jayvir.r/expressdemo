const request = require('request');

const forecast = (latitude, longtitude, callback) => {
  const url =
    'https://api.darksky.net/forecast/c34bf5425a2a3e54994fbba76a591b5a/' +
    latitude +
    ',' +
    longtitude;

  request({ url, json: true }, (error, { body }) => {
    if (error) {
      callback('Unable to Connect to Weather Services', undefined);
    } else if (body.error) {
      callback('Unable to find location', undefined);
    } else {
      callback(
        undefined,
        body.daily.data[0].summary +
          ' it is currently ' +
          body.currently.temperature +
          ' degress out. Min Temperature:' +
          body.daily.data[0].temperatureLow +
          'Max Temperature:' +
          body.daily.data[0].temperatureHigh +
          '. There is ' +
          body.currently.precipIntensity +
          '% of chance rain.'
      );
    }
  });
};

module.exports = forecast;
